#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void limpaTela();
void limparBuffer();
int *criaVetor(int t);
void copiaVetor(int *src, int *des, int t);
void preencheVetor(int *v, int t);
void printVetor(int *v, int t, int m);
void maxHeapfy(int *v, int i, int tam, unsigned long int *it);
void buildHeap(int *v, int tam, unsigned long int *it);
void heapSort(int *v, int tam);
int binSearch(int *v, int t, int query, double *tc);
int binSearchImp(int *v, int t, int query, double *ti);


int main(int argc, char *argv[]) {
	limpaTela();
	int *x;
	int tam, q, rescomum, restunada, tmp, cont = 0;;
	double tcomum = 0.0, timpro = 0.0;
	/*do {
		printf("\nInsira o tamanho do vetor e na sequencia sera gerado um vetor aleatorio\n");
		printf("Valores acima de 2 apenas!\n");
		printf("Tamanho: ");
		limparBuffer();
		scanf("%d", &tam);
		limparBuffer();
		printf("\n");
		if (tam < 2) printf("\nO vetor tem que ter mais do que 2 elementos pfvr\n");
	} while (tam < 2);*/
	for (tam = 1000; tam <= 1000000; tam += 1000) {
		cont++;
		x = criaVetor(tam);
		preencheVetor(x, tam);
		printf("cont = %d\n",cont);
		//printf("VETOR GERADO:\n");
		heapSort(x, tam);
		//printVetor(x, tam, -1);
		srand(time(NULL));
		q = ((rand() % (100 + (rand() % 400))) + 1);
		printf("Valor a ser procurado: %d\n", q);
		rescomum = binSearch(x, tam, q, &tcomum);
		if (rescomum < 0) printf("\nValor: %d nao encontrado.\n", q);
		else printf("\nValor: %d foi encontrado na posicao %d\n", q, rescomum);
		restunada = binSearchImp(x, tam, q, &timpro);
		if (restunada < 0) printf("\nValor: %d nao encontrado.\n", q);
		else printf("\nValor: %d foi encontrado na posicao %d\n", q, restunada);


		free(x);
	}
	
	printf("\nFim do role\n");
	printf("Tempo total busca comum: %lf\n", tcomum);
	printf("Tempo medio busca comum: %lf\n", (tcomum / cont));
	printf("Tempo total busca tunada: %lf\n", timpro);
	printf("Tempo medio busca tunada: %lf\n", (timpro / cont));
	return 0;
}

void limpaTela(){
    /*Antes de realizara função de limpar a tela
     *checa qual o sistema operacional, visto que
     *cada um tem uma forma de limpar a tela */
    #ifdef _WIN32
        system("cls");
    #elif defined(unix) || defined(__unix__) || defined(__unix) || (defined(__APPLE__) && defined(__MACH__))
        system("clear");
#else
    #error "S.O. nao suportado."
 
#endif
}

void limparBuffer() {
    /*Antes de realizara função de limpar o buffer do teclado
     *checa qual o sistema operacional, visto que
     *cada um tem uma forma de limpar o buffer */
    #ifdef _WIN32
        fflush(stdin);
    #elif defined(unix) || defined(__unix__) || defined(__unix) || (defined(__APPLE__) && defined(__MACH__))
        fpurge(stdin);
#else
    #error "S.O. nao suportado."
#endif
}

int * criaVetor(int t) {
	int *p;
	p = (int*)malloc(t * sizeof(int));
	if (!p) {
		printf("\nAlocacao de memoria deu problema.\n");
		exit(-1);
	}
	return p;
}

void copiaVetor(int *src, int *des, int t) {
	int i;
	for (i = 0; i < t; i++) {
		des[i] = src[i];
	}
}

void preencheVetor(int *v, int t) {
	int i;
	srand(time(NULL));
	for (i = 0; i < t; i++) {
		v[i] = ((rand() % (100 + (rand() % t))) + 1);
	}
}

void printVetor(int *v, int t, int m) {
	int i;
	for (i = 0; i < t; i++) {
		if (i == m) printf("*%d*|", v[i]);
		else printf(" %d |", v[i]);
	}
	printf("\n");
}

void maxHeapfy(int *v, int i, int tam, unsigned long int *it) {
	int maior = i, left = 2 * i + 1, right = 2 * i + 2, temp;
	if (left < tam && v[left] > v[maior]) maior = left;
	if (right < tam && v[right] > v[maior]) maior = right;
	if (maior != i) {
		(*it)++;
		temp = v[i];
		v[i] = v[maior];
		v[maior] = temp;
		maxHeapfy(v, maior, tam, it);
	}
}
void buildHeap(int *v, int tam, unsigned long int *it) {
	int i, n, temp;

	for (i = (tam / 2) - 1; i >= 0; i--) {
		maxHeapfy(v, i, tam, it);
	}
	for (n = tam - 1; n >= 1; n--) {
		(*it)++;
		temp = v[0];
		v[0] = v[n];
		v[n] = temp;
		maxHeapfy(v, 0, n, it);
	}
}
void heapSort(int *v, int tam) {
	unsigned long int iteraction = 0;
	clock_t start, end;
	double totalTime;
	start = clock();
	buildHeap(v, tam, &iteraction);
	end = clock();
	totalTime = (((double) (end - start)) * 1000) / CLOCKS_PER_SEC;

}

int binSearch(int *v, int t, int query, double *tc) {
	clock_t start, end;
	double totalTime;
	start = clock();
	int l = 0, r = (t - 1), m;

	while (r >= l) {
		 
        m = l + (r - l) / 2;
        if (v[m] == query) {
        	end = clock();
        	totalTime = (((double) (end - start)) * 1000) / CLOCKS_PER_SEC;
        	(*tc) += totalTime;
        	printf("\nTEMPO BUSCA COMUM yess: %lf ms\n", totalTime);
            return m; 
        }
        if (v[m] > query) r = m - 1;
        else if (v[m] < query) l = m + 1;

    } 
    end = clock();
    totalTime = (((double) (end - start)) * 1000) / CLOCKS_PER_SEC;
    (*tc) += totalTime;
    printf("\nTEMPO BUSCA COMUM nooooo: %lf ms\n", totalTime);
    return -1; 
}

int binSearchImp(int *v, int t, int query, double *ti) {
	clock_t start, end;
	double totalTime;
	int l = 0, r = (t - 1), m, divide = (t / 4);
	start = clock();
	if ((query - v[l]) < (v[r] - query)) {
		m = divide;
		r = divide + divide - 1;
	} else { 
		m = r - divide;
		l = divide + divide - 1;
	}
	while (r >= l) {
		 
        
        if (v[m] == query) {
        	end = clock();
        	totalTime = (((double) (end - start)) * 1000) / CLOCKS_PER_SEC;
        	(*ti) += totalTime;
        	printf("\nTEMPO BUSCA TUNADA yess: %lf ms\n ", totalTime);
            return m; 
        }
        if (v[m] > query) r = m - 1;
        else if (v[m] < query) l = m + 1;
        m = l + (r - l) / 2;

    } 
    end = clock();
    totalTime = (((double) (end - start)) * 1000) / CLOCKS_PER_SEC;
    (*ti) += totalTime;
    printf("\nTEMPO BUSCA TUNADA nooooo: %lf ms\n", totalTime);
    return -1; 

}





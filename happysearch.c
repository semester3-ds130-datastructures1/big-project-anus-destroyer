#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void limpaTela();
void limparBuffer();
int *criaVetor(int t);
void copiaVetor(int *src, int *des, int t);
void preencheVetor(int *v, int t);
void printVetor(int *v, int t, int m);
void maxHeapfy(int *v, int i, int tam, unsigned long int *it);
void buildHeap(int *v, int tam, unsigned long int *it);
void heapSort(int *v, int tam);
int binSearch(int *v, int t, int query, double *tc);
int binSearchImp(int *v, int t, int query, double *tt);


int main(int argc, char *argv[]) {
	limpaTela();
	int *x;
	int tam, q, rescomum, restunada;
	double tempocomum = 0, tempotunning = 0;
	do {
		printf("\nInsira o tamanho do vetor e na sequencia sera gerado um vetor aleatorio\n");
		printf("Valores acima de 2 apenas!\n");
		printf("Tamanho: ");
		limparBuffer();
		scanf("%d", &tam);
		limparBuffer();
		printf("\n");
		if (tam < 2) printf("\nO vetor tem que ter mais do que 2 elementos pfvr\n");
	} while (tam < 2);
	
	x = criaVetor(tam);
	preencheVetor(x, tam);
	printf("VETOR GERADO:\n");
	printVetor(x, tam, -1);
	heapSort(x, tam);
	//printf("\nVETOR ORENADO (HEAP SORT)\n");
	//printf("\n");
	srand(time(NULL));
	q = ((rand() % (100 + (rand() % 400))) + 1);
	printf("Valor a ser procurado: %d\n", q);
	//printf("\nBUSCANDO PELA PESQUISA BINARIA COMUM");
	rescomum = binSearch(x, tam, q, &tempocomum);
	if (rescomum < 0) printf("\nValor: %d nao encontrado.\n", q);
	else printf("\nValor: %d foi encontrado na posicao %d\n", q, rescomum);
	//printf("\nBUSCANDO PELA PESQUISA BINARIA TUNADA");
	restunada = binSearchImp(x, tam, q, &tempotunning);
	if (restunada < 0) printf("\nValor: %d nao encontrado.\n", q);
	else printf("\nValor: %d foi encontrado na posicao %d\n", q, restunada);


	free(x);
	
	printf("\nFim do role\n");
	printf("\nMedia de tempo comum: %lf", (tempocomum / 100));
	printf("\nMedia de tempo tuning: %lf", (tempotunning / 100));
	return 0;
}

void limpaTela(){
    /*Antes de realizara função de limpar a tela
     *checa qual o sistema operacional, visto que
     *cada um tem uma forma de limpar a tela */
    #ifdef _WIN32
        system("cls");
    #elif defined(unix) || defined(__unix__) || defined(__unix) || (defined(__APPLE__) && defined(__MACH__))
        system("clear");
#else
    #error "S.O. nao suportado."
 
#endif
}

void limparBuffer() {
    /*Antes de realizara função de limpar o buffer do teclado
     *checa qual o sistema operacional, visto que
     *cada um tem uma forma de limpar o buffer */
    #ifdef _WIN32
        fflush(stdin);
    #elif defined(unix) || defined(__unix__) || defined(__unix) || (defined(__APPLE__) && defined(__MACH__))
        fpurge(stdin);
#else
    #error "S.O. nao suportado."
#endif
}

int * criaVetor(int t) {
	int *p;
	p = (int*)malloc(t * sizeof(int));
	if (!p) {
		printf("\nAlocacao de memoria deu problema.\n");
		exit(-1);
	}
	return p;
}

void copiaVetor(int *src, int *des, int t) {
	int i;
	for (i = 0; i < t; i++) {
		des[i] = src[i];
	}
}

void preencheVetor(int *v, int t) {
	int i;
	srand(time(NULL));
	for (i = 0; i < t; i++) {
		v[i] = ((rand() % (100 + (rand() % t))) + 1);
	}
}

void printVetor(int *v, int t, int m) {
	int i;
	for (i = 0; i < t; i++) {
		if (i == m) printf("*%d*|", v[i]);
		else printf(" %d |", v[i]);
	}
	printf("\n");
}

void maxHeapfy(int *v, int i, int tam, unsigned long int *it) {
	int maior = i, left = 2 * i + 1, right = 2 * i + 2, temp;
	if (left < tam && v[left] > v[maior]) maior = left;
	if (right < tam && v[right] > v[maior]) maior = right;
	if (maior != i) {
		(*it)++;
		temp = v[i];
		v[i] = v[maior];
		v[maior] = temp;
		maxHeapfy(v, maior, tam, it);
	}
}
void buildHeap(int *v, int tam, unsigned long int *it) {
	int i, n, temp;

	for (i = (tam / 2) - 1; i >= 0; i--) {
		maxHeapfy(v, i, tam, it);
	}
	for (n = tam - 1; n >= 1; n--) {
		(*it)++;
		temp = v[0];
		v[0] = v[n];
		v[n] = temp;
		maxHeapfy(v, 0, n, it);
	}
}
void heapSort(int *v, int tam) {
	unsigned long int iteraction = 0;
//	printf("+----------------------------------------+\n");
//	printf("Metodo de HEAPSORT\n");
	clock_t start, end;
	double totalTime;
	start = clock();
	buildHeap(v, tam, &iteraction);
	end = clock();
	totalTime = (((double) (end - start)) * 1000) / CLOCKS_PER_SEC;
//	printf("Iteracoes: %ld\n", iteraction);
//	printf("Tempo total: %lf segundos\n", totalTime);
//	printf("\nFim do metodo de HEAPSORT\n");
//	printf("+---------------------------------------+\n");
}

int binSearch(int *v, int t, int query, double *tc) {
	clock_t start, end;
	double totalTime;
	start = clock();
	int l = 0, r = (t - 1), m;

	while (r >= l) {
		 
        m = l + (r - l) / 2;
        //printf("L = %d | R = %d | M = %d\n", l, r, m);
        //printf("v[m] = %d | query = %d\n", v[m], query);
        //printVetor(v, t, m);
        if (v[m] == query) {
        	end = clock();
        	totalTime = (((double) (end - start)) * 1000) / CLOCKS_PER_SEC;
        	(*tc) = (*tc) + totalTime;
        	printf("\nTEMPO BUSCA COMUM yess: %lf\n", totalTime);
            return m; 
        }
        if (v[m] > query) r = m - 1;
        else if (v[m] < query) l = m + 1;

    } 
    end = clock();
    totalTime = (((double) (end - start)) * 1000) / CLOCKS_PER_SEC;
    (*tc) = (*tc) + totalTime;
    printf("\nTEMPO BUSCA COMUM nooooo: %lf\n", totalTime);
    return -1; 
}

int binSearchImp(int *v, int t, int query, double *tt) {
	clock_t start, end;
	double totalTime;
	int res;
	start = clock();
	int l = 0, r = (t - 1), m;
	
	m = l + (r - l) / 2;
	res = query - v[m];
	if (res > 0) l = m + 1;
	else if (res < 0) r = m - 1;
	else {
		end = clock();
		totalTime = (((double) (end - start)) * 1000) / CLOCKS_PER_SEC;
		(*tt) = (*tt) + totalTime;
		printf("\nTEMPO BUSCA TUNADA FUCKER: %lf\n", totalTime);
		return (m);
	}

	
		while (r >= l) { 
        m = l + (r - l) / 2;
        //printf("L = %d | R = %d | M = %d\n", l, r, m);
        //printf("v[m] = %d | query = %d\n", v[m], query);
        //printVetor(v, t, m);
        if (v[m] == query) {
        	end = clock();
        	totalTime = (((double) (end - start)) * 1000) / CLOCKS_PER_SEC;
        	(*tt) = (*tt) + totalTime;
        	printf("\nTEMPO BUSCA TUNADA ok: %lf\n", totalTime);
            return m; 
        }
        if (v[m] > query) r = m - 1;
        else if (v[m] < query) l = m + 1;

    } 
    end = clock();
    totalTime = (((double) (end - start)) * 1000) / CLOCKS_PER_SEC;
    (*tt) = (*tt) + totalTime;
    printf("\nTEMPO BUSCA TUNADA aff: %lf\n", totalTime);
    return -1; 
	

}




